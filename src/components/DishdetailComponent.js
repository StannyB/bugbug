import React from 'react';
import {
  Card,
  CardImg,
  CardImgOverlay,
  CardText,
  CardBody,
  CardTitle,
} from 'reactstrap';

function RenderDish({dish}) {
  if (dish != null) {
    return (
      <div className="col-12 col-md-5 mt-1">
        <Card>
          <CardImg top src={dish.image} alt={dish.name} />
          <CardBody>
            <CardTitle>{dish.name}</CardTitle>
            <CardText>{dish.description}</CardText>
          </CardBody>
        </Card>
      </div>
    );
  } else return <div />;
}
function RenderComments({comments}) {
  if (comments == null) {
    return <div />;
  } else {
    const Comments = comments.map (comment => (
      <div className="row">
        <div className="col-12 mt-2">
          {comment.comment}
        </div>
        <div className="col-12 mt-2">
          --{comment.author} &nbsp;
          {new Date (comment.date).toLocaleDateString ()}
        </div>
      </div>
    )); 
    return (
      <div className="col-12 col-md-5 mt-1">
        <h2> Comments </h2>

        <ul className="list-unstyled">
          {Comments}
        </ul>
      </div>
    );
  }
}
const DishDetail = props => {
  console.log(props.comments);
  return (
    <div className="container">
      <div className="row">
        <RenderDish dish={props.dish} />
        <RenderComments comments={props.comments} />
      
      </div>
    </div>
  );
};
export default DishDetail;
